package com.beans.model;

import java.io.Serializable;

public class Availability implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2228061421037739921L;
	private String name;
	private boolean available;
	public Availability(String name, boolean available) {
	this.name = name;
	this.available = available;
	}
	public boolean isAvailable() {
	return available;
	}
	public String getName() {
	return name;
	}

}
